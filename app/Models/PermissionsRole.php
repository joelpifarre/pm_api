<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isEmpty;
use function PHPUnit\Framework\isNull;

class PermissionsRole extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'permission',
        'role',
        'has',
    ];


    // public static  function roleHasPermission($permission)
    // {
    //     $user = Auth::user();

    //     $company = Employee::select('company','role')->where('user', '=', $user->id)->where('isCurrent', '=', true)->first();
    //     if ($company == null || empty($company)) {
    //         return false;
    //     }
    //     error_log($company->role);
    //     error_log($permission);
    //     $pr = self::where('permission', '=', $permission)->where('role', '=', $company->role)->first();
    //     error_log($pr);
    //     if ($pr == null || empty($pr) || !$pr->has) {
    //         return false;
    //     }
    //     return true;
    // }

}
