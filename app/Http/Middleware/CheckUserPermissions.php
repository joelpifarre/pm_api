<?php

namespace App\Http\Middleware;

use App\Models\Employee;
use App\Models\PermissionsRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckUserPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $permission)
    {
        $user = Auth::user();
        $badResponse = [
            "success" => false,
            "message" => "Unauthorized"
        ];

        $company = Employee::select('company', 'role')->where('user', '=', $user->id)->where('isCurrent', '=', true)->first();
        if ($company == null || empty($company)) {
            return  response()->json($badResponse,401);
        }
        error_log($company->role);
        error_log($permission);
        $pr = PermissionsRole::where('permission', '=', $permission)->where('role', '=', $company->role)->first();
        error_log($pr);
        if ($pr == null || empty($pr) || !$pr->has) {
            return  response()->json($badResponse, 401);
        }

        return $next($request);
    }
}
