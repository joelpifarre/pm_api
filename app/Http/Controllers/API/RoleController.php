<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PermissionsRole;
use App\Models\Role;
use Illuminate\Http\Request;
use  Validator;


class RoleController extends BaseController
{
    //
    public function get(Request $request)
    {
    }

    public function list(Request $request)
    {
        // $can = $this->checkRole('read_role');
        // $can = PermissionsRole::roleHasPermission('read_role');

        // if (!$can) {
        //     return $this->sendError('Validation Error', 'Validation Error', 401);
        // }
        return $this->sendResponse(Role::all(), 'Role list');
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "code" => "required",
            "description" => "required",
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }
        $input = $request->all();
        $role = Role::create($input);
        $success['name'] = $role->code;
        return $this->sendResponse($input, 'Role created succesfully');
    }

    public function update(Request $request)
    {

    }

    public function delete(Request $request)
    {

    }

}
