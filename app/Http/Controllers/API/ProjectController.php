<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjectController extends Controller
{
    //
    public function create(Request $request){}
    public function read(Request $request){}
    public function get(Request $request){}
    public function update(Request $request){}
    public function delete(Request $request){}
}
