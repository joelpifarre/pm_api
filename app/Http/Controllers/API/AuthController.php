<?php

namespace App\Http\Controllers\API;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
class AuthController extends BaseController
{

    public function login(Request $request){
        if(empty($request->email) || empty($request->password)){
            return $this->sendError('Unauthorised.', ['error' => 'Unauthorised'], 401);
        }
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            if (!$user->status){
                return $this->sendError('Unauthorised.', ['error'=>'Unauthorised'],401);
            }
            $success['name'] =  $user->name;
            $company = Employee::select('company')->where('user','=',$user->id)->where('isCurrent','=',true)->first();
            if($company == null || empty($company)){
                $company = Employee::select('company')->where('user', '=', $user->id)->first();
            }

            if($company != null){
                $curr = Employee::where('user','=',$user->id)->where('company','=',$company->company)->first();
                $curr->isCurrent = true;
                $curr->save();
                $currCompany = $curr->id;
            } else {
                 $currCompany = null;
            }
            $token = $user->createToken('app')->accessToken;
            // $success['token'] =  $user->createToken('app')->accessToken;
            $success['company'] = $currCompany;
            return $this->sendResponseLogin($success, 'User login succesfully',$token);
        }
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            "name" => "required",
            "surname"=>"required",
            "email" => "required|email",
            "password" => "required",
            "c_password" => "required|same:password",
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['status']=true;
        $user = User::create($input);
        $success['name'] = $user->name;

        return $this->sendResponse($input, 'User created succesfully');
    }


}

