<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\RoleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
});


Route::prefix('admin')->middleware('auth:api')->group(function () {
    Route::prefix('role')->group(function () {
        Route::post('create', [RoleController::class, 'create'])->middleware('role:create_role');
        Route::get('list', [RoleController::class, 'list'])->middleware('role:read_role');
    });

});



