<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_permissions = [
            'create',
            'read',
            'update',
            'delete',
        ];
        for ($i=0; $i < count($role_permissions) ; $i++) {
            # code...
            DB::table('permissions')->insert([
                'code' => $role_permissions[$i] . '_role',
                'description' => $role_permissions[$i] . ' role'
            ]);
        }

        DB::table('roles')->insert([
            'code'=>'adm',
            'description' => 'administrator'
        ]);

        for ($i=0; $i < count($role_permissions) ; $i++) {
            # code...
            DB::table('permissions_roles')->insert([
                'permission'=> $role_permissions[$i] . '_role',
                'role'=>'adm',
                'has'=>true
            ]);
        }





    }
}
