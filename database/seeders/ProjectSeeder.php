<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $project_permissions = [
            'create',
            'delete',
            'read',
            'update',
        ];

        for ($i=0; $i < count($project_permissions) ; $i++) {
            DB::table('permissions')->insert([
                'code' => $project_permissions[$i] . '_project',
                'description' => $project_permissions[$i] . ' project'
            ]);
        }

        for ($i=0; $i < count($project_permissions) ; $i++) {
            DB::table('permissions_roles')->insert([
                'permission'=> $project_permissions[$i] . '_project',
                'role'=>'adm',
                'has'=>true
            ]);
        }

    }
}
