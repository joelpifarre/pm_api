<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $user_permissions = [
            'create',
            'read',
            'update',
            'delete',
        ];
        for ($i = 0; $i < count($user_permissions); $i++) {
            # code...
            DB::table('permissions')->insert([
                'code' => $user_permissions[$i] . '_user',
                'description' => $user_permissions[$i] . ' user'
            ]);
        }

        for ($i = 0; $i < count($user_permissions); $i++) {
            # code...
            DB::table('permissions_roles')->insert([
                'permission' => $user_permissions[$i] . '_user',
                'role' => 'adm',
                'has' => true
            ]);
        }


        DB::table('users')->insert([
            'name' => 'admin',
            'email' =>'admin@admin.com',
            'password' => bcrypt('admin'),
            'status'=>true,
            'canDelete' => false
        ]);



    }
}
