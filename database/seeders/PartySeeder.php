<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $party_permissions = [
             'create',
             'delete',
             'read',
             'update',
         ];

         for ($i = 0; $i < count($party_permissions); $i++) {
             # code...
             DB::table('permissions')->insert([
                 'code' => $party_permissions[$i] . '_party',
                 'description' => $party_permissions[$i] . ' party'
             ]);
         }

         for ($i = 0; $i < count($party_permissions); $i++) {
             # code...
             DB::table('permissions_roles')->insert([
                 'permission' => $party_permissions[$i] . '_party',
                 'role' => 'adm',
                 'has' => true
             ]);
         }

        DB::table('parties')->insert([
            'name' => 'Party Test',
            'nif' => '1234567',
        ]);
        DB::table('companies')->insert([
            'party' => 1,
            'nif' => '1234567',
            'work_hours'=>8.0
        ]);
        DB::table('employees')->insert([
            'company' => 1,
            'user' => 1,
            'role'=>'adm',
            'isCurrent'=>false
        ]);

    }
}
