<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->unsignedInteger('customer');
            $table->foreign('customer')->references('id')->on('parties');
            $table->unsignedInteger('company');
            $table->foreign('company')->references('id')->on('companies');
            $table->string('reference');
            $table->string('party_password');
            $table->enum('status',['pending','started','testing','done','closed','cancelled']);
            $table->timestamps();
        });


        // DB::statement('ALTER TABLE items MODIFY COLUMN quoted INTERVAL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
