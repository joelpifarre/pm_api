<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('company');
            $table->foreign('company')->references('id')->on('companies');
            $table->unsignedInteger('user');
            $table->foreign('user')->references('id')->on('users');
            $table->string('role');
            $table->foreign('role')->references('code')->on('roles');
            $table->timestamps();
            $table->boolean('isCurrent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
